#!/bin/sh
# File: update-ipasam-rpm.sh
# Location: https://gitlab.com/bgstack15/stackrpms/
# Author: bgstack15
# Startdate: 2022-05-05 10:09
# SPDX-License-Identifier: GPL-3.0
# Title: Update ipasam rpm
# Project: update-ipasam-rpm
# Purpose: Build new ipasam package when ipa-server-trust-ad increments
# History:
#    2022-12-01 improve to handle current page design, and dynamically learn all copr chroots to exclude
# Usage:
#    on AlmaLinux 8 system (storage3) in cron.
# Dependencies:
#    file ~/.config/copr with contents described from https://copr.fedorainfracloud.org/api/
#    plecho from bgscripts
#    Multiple variables in ~/.config/ipasam: mirror_path and spec_url

test -e ~/.config/ipasam && . ~/.config/ipasam
test -z "${old_ver_file}" && old_ver_file=~/.cache/ipa-server-trust-ad.ver
old_ver_fd="$( dirname "${old_ver_file}" )"
# Path to web directory that contains ipa-server-trust-ad rpm files. Have to do it this way because ipa-server-trust-ad package is in different dnf module than what this server uses so it is not visible from dnf.
test -z "${mirror_path}" && mirror_path="http://www.example.com/mirror/almalinux/8/AppStream/x86_64/os/Packages/"
test -z "${spec_url}" && spec_url="https://gitlab.com/bgstack15/stackrpms/-/raw/ipasam-bump/ipasam/ipasam.spec"
test -z "${logfile}" && logfile=~/log/copr-ipasam.log
test -z "${coprrepo}" && coprrepo=stackrpms
logfd="$( dirname "${logfile}" )"

test ! -d "${logfd}" && mkdir -p "${logfd}"
{
   # compare old to new version
   # get old version
   old_ver="$( cat "${old_ver_file}" 2>/dev/null )"
   # get newest version available
   page="$( curl "${mirror_path}" --silent )"
   #latest_file="$( echo "${page}" | awk -F'>' '/ipa-server-trust-ad/{print $2}' | awk -F'"' '{print $2}' | sort --version-sort | tail -n1 )"
   latest_file="$( echo "${page}" | awk '/ipa-server-trust-ad/' | grep -oE '"ipa-server-trust-ad.*\.rpm"' | tr -d '"'| sort --version-sort | tail -n1 )"
   # Awk $5 because package name takes first four columns when splitting with dash
   latest_ver="$( echo "${latest_file}" | awk -F'-' 'BEGIN{OFS="-"} {print $5}' )"
   latest_rel="$( echo "${latest_file}" | awk -F'-' '{print $6}' | awk -F'.' '{print $1}' )"
   echo "${latest_ver}-${latest_rel}"
   new_ver="${latest_ver}-${latest_rel}"
   # if not the same, do stuff
   if test "${new_ver}" != "${old_ver}" ;
   then
      echo "Need to do stuff, because new ${new_ver} != ${old_ver}"
      cd ~/.cache # use cache directory
      rm ipasam.spec ; wget "${spec_url}"
      sed -i -r ipasam.spec \
         -e "/%define samver\>/s/%\(.*$/${latest_ver}/;" \
         -e "/%define samrel\>/s/%\(.*$/${latest_rel}/;"
      rpmbuild --nodeps -bs ipasam.spec && {
         available_chroots="$( copr list bgstack15 | awk -v "name=${coprrepo}" '/Name:/ && $0 ~ name {a=1} /Name:/ && $0 !~ name {a=0} /results/{if(a==1)print $1;}' | tr -d ':' )"
         # exclude all chroots except epel-7-x86_64 epel-8-x86_64
         excluded_chroots="$( echo "${available_chroots}" | grep -vE 'epel-[78]-x86_64' )"
         copr build $( for word in ${excluded_chroots} ; do printf "%s %s" "--exclude-chroot" "${word}" )  --nowait "${coprrepo}" ~/rpmbuild/SRPMS/ipasam-${new_ver}.src.rpm
         test ! -d "${old_ver_fd}" && mkdir p "${old_ver_fd}"
         echo "${new_ver}" > "${old_ver_file}"
         rm ~/.cache/ipasam.spec
      }
   else
      echo "Current version already: ${new_ver}"
   fi
} 2>&1 | /usr/bin/plecho | tee -a "${logfile}"
