# Project: update-ipasam-rpm
%define upstream ipa-server-trust-ad
%define samso %( rpm -ql %{upstream} 2>/dev/null | grep ipasam )
# For the srpm generation on a local machine, ipasam.so is absent
# but that is OK because we will build the real ipasam package
# in copr.
%if "%{samso}" == ""
%define samso /usr/lib64/samba/pdb/ipasam.so
%define error1 1
%endif
%define samsodir %( dirname %{samso} )
%define samver %( rpm -q --qf '%%{version}' %{upstream} )
%define samrel %( rpm -q --qf '%%{release}' %{upstream} )
Name:		ipasam
Version:	%{samver}
Release:	%{samrel}
Summary:	Just the ipasam.so file
Provides: %{upstream}
Conflicts: %{upstream}

License:	GPL 3.0
URL:		https://gitlab.com/bgstack15/ipasam/
#Source0:	ipasam.spec

BuildRequires:	%{upstream}
#Requires:	

%description
To use samba with ipa user resolution, you need ipasam.so
which normally comes from %{upstream} but that has many
extraneous dependencies. Samba needs only the ipasam.so
file, so this package has just that file.

%prep
%if 0%{?error1}
echo "Info: ipasam.so is absent. This is normal only for the local system srpm preparation." 1>&2
echo "The file needs to exist for copr builds." 1>&2
%endif
:

%build
:

%install
%{__install} --directory -m0755 %{buildroot}%{samsodir}
%{__install} -m0755 %{samso} %{buildroot}%{samso}

%files
%{samso}
%doc

%changelog
* Thu May 05 2022 B. Stack <bgstack15@gmail.com> - %{samver}
- Initial release
